from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'My New App'
settings.subtitle = 'powered by web2py'
settings.author = 'Pratik'
settings.author_email = 'pratik@bhoir.org'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'ef0f4500-6c9a-407d-bae1-71d9dd554642'
settings.email_server = 'logging'
settings.email_sender = 'pratik@bhoir.org'
settings.email_login = 'pratik@bhoir.org'
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
