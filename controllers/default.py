# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict(form=auth())

def winners():
    winner = db().select(db.auth_user.first_name, limitby=(0, 3), orderby='<random>')
    return dict(winner=winner)

def error():
    return dict()
